FROM python:3

WORKDIR /usr/src/app

COPY requirements.txt ./

RUN apt-get update && \
    apt-get -y install gcc mono-mcs && \
    rm -rf /var/lib/apt/lists/*

RUN pip install --no-cache-dir -r requirements.txt

COPY . .

CMD [ "python", "./server.py" ]

#BUILD docker build -t dataspartan/pythonserver .
#RUN docker run -p 50051:50051 dataspartan/pythonserver