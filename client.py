from __future__ import print_function
import grpc
import tweetClassifier_pb2
import tweetClassifier_pb2_grpc
from optparse import OptionParser

parser = OptionParser()
parser.add_option("-b", "--body", dest="body",
                  help="body of the tweet", 
                  default="This tweet may or may not have positive emotion") 
parser.add_option("-i", "--id",
                  dest="id", default="1234-5678-uuid",
                  help="id of the tweet")
(options, args) = parser.parse_args()


def send_tweet(stub, id, body):
    payload = tweetClassifier_pb2.Tweet(id=id, body=body)
    response = stub.ClassifyTweet(payload)  
    print("server response:\n================\n", response)

def run():
    print("your input:", options)
    channel = grpc.insecure_channel('localhost:50051')
    stub = tweetClassifier_pb2_grpc.ClassifierStub(channel)
    send_tweet(stub,options.id,options.body)

if __name__ == '__main__':
  run()
