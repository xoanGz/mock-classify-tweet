"""GRPC server implementation for a simple recommendation engine."""

from concurrent import futures
import time
import grpc
import tweetClassifier_pb2
import tweetClassifier_pb2_grpc
import random

_ONE_DAY_IN_SECONDS = 60 * 60 * 24

class ClassifierServicer(tweetClassifier_pb2_grpc.ClassifierServicer):
    def __init__(self):
        random.seed(1337)
    def ClassifyTweet(self, request, context):
        prediction = random.randint(1,4)#return a random class from [1,...,4]
        id = request.id
        return tweetClassifier_pb2.ClassificationReply(id=id, class_prediction = prediction)

def serve():
  server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
  tweetClassifier_pb2_grpc.add_ClassifierServicer_to_server(
      ClassifierServicer(), server)
  server.add_insecure_port('[::]:50051')
  server.start()
  try:
    while True:
      time.sleep(_ONE_DAY_IN_SECONDS)
  except KeyboardInterrupt:
    server.stop(0)

if __name__ == '__main__':
  serve()